import { CHANGE_PLAYER_TARGETED, CHANGE_STARTED, ADD_COUNTER, SUB_COUNTER, PLACE_MOVE, CHANGE_PLAYER, RESTART_GAME } from '../constants/action_types'

export const add = () => ({
    type: ADD_COUNTER
})

// x = "x", o = "o"
export const placeMove = (payload) => ({
    type: PLACE_MOVE,
    player: payload.player,
    index: payload.index
})

export const changePlayer = () => ({
    type: CHANGE_PLAYER,
})

export const changePlayerTargeted = (payload) => ({
    type: CHANGE_PLAYER_TARGETED,
    player: payload
})

export const restartGame = () => ({
    type: RESTART_GAME
})

export const changeStarted = () => ({
    type: CHANGE_STARTED
})