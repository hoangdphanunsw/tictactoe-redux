import { CHANGE_PLAYER, CHANGE_PLAYER_TARGETED } from "../constants/action_types"

const player = (state = "x", action) => {
    switch (action.type) {
        case CHANGE_PLAYER:
            if (state === "x") {
                return "o"
            } else {
                return "x"
            }
        case CHANGE_PLAYER_TARGETED:
            return action.player
        default:
            return state
    }
}

export default player