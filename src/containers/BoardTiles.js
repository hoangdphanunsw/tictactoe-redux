import React, { useState } from 'react'
import { button } from 'react'
import { connect } from 'react-redux'
import { add, placeMove, changePlayer, restartGame, changeStarted, changePlayerTargeted } from '../actions/index'
import { Paper, CardActionArea, Grid, Typography, Button } from "@material-ui/core"


// instead of having { dispatch } we just put the connected function in.

function BoardTiles({ state, placeMove, changePlayer, restartGame, changeStarted, changePlayerTargeted }) {
    const [winner, setWinner] = useState("")

    const handleMove = (index) => {
        if (state.board[index] == 0) {
            placeMove({
                player: state.player,
                index: index
            })
            let winner = checkWin()
            if (winner === 'x') {
                setWinner("x has won")
            } else if (winner === 'o') {
                setWinner("o has won")
            } else {
                changePlayer()
            }
        }
    }

    // check if win occurs. returns player who won.
    const checkWin = () => {
        // check rows first
        for (let i = 0; i < 9; i += 3) {
            if (state.board[i] == state.board[i+1] && state.board[i] == state.board[i+2] && state.board[i] != 0) {
                return state.board[i]
            }
        }
        // check columns
        for (let i = 0; i < 4; ++i) {
            if (state.board[i] == state.board[i+3] && state.board[i] == state.board[i+6] && state.board[i] != 0) {
                return state.board[i]
            }
        }
        // check diagonals
        if (state.board[0] == state.board[4] && state.board[0] == state.board[8] && state.board[0] != 0) {
            return state.board[0]
        }

        if (state.board[2] == state.board[4] && state.board[2] == state.board[6] && state.board[2] != 0) {
            return state.board[2]
        }

    }

    const handleRestart = () => {
        // console.log(state)
        restartGame()
        setWinner("")
        if (state.started === "x") {
            changePlayerTargeted("o")
        } else {
            changePlayerTargeted("x")
        }
        changeStarted()
    }

    function Square(index) {
        // first need to check if the space is already taken
        let label
        if (state.board[index] == 0) {
            label = ""
        } else {
            label = state.board[index]
        }
        return (
            <Grid item xs={4}>
                <CardActionArea
                    onClick={() => {
                        handleMove(index)
                    }}
                >
                    <Paper style={{display: "flex", width: 150, height: 150, textAlign: "center", justifyContent: "center", fontSize: 40, alignItems: "center", textJustify: "center"}}>
                            { label }
                    </Paper>
                </CardActionArea>
            </Grid>

        )
    }

    return (
        <div style={{alignItems: "center", justifyContent: "center", justifyItems: "center", width: 450}}>
            <div style={{padding: 30}}>
                <Typography style={{fontSize: 30}}>
                    { state.player }'s Turn
                </Typography>
            </div>
            <Grid container spacing={0}>
                    {Square(0)}
                    {Square(1)}
                    {Square(2)}
                    {Square(3)}
                    {Square(4)}
                    {Square(5)}
                    {Square(6)}
                    {Square(7)}
                    {Square(8)}
            </Grid>
            <div style={{padding: 30}}>
                <Button
                    onClick={() => {handleRestart()}}
                >
                    Restart Game
                </Button>
                <Typography style={{fontSize: 40}}>
                    { winner }
                </Typography>
            </div>
            
        </div>
    )
}

const mapStateToProps = state => ({
    state: state
})
  
const mapDispatchToProps = dispatch => ({
    placeMove: (player, index) => dispatch(placeMove(player, index)),
    changePlayer: () => dispatch(changePlayer()),
    restartGame: () => dispatch(restartGame()),
    changeStarted: () => dispatch(changeStarted()),
    changePlayerTargeted: (player) => dispatch(changePlayerTargeted(player))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BoardTiles)