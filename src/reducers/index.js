import { combineReducers } from 'redux'
import add from './add'
import board from './board'
import player from './player'
import started from './started'

export default combineReducers({
    add,
    board,
    player,
    started
})