import { CHANGE_STARTED } from '../constants/action_types'

const changeStarted = (state = "x", action) => {
    switch (action.type) {
        case CHANGE_STARTED:
            if (state == "x") {
                return "o"
            } else {
                return "x"
            }
        default:
            return state
    }
}

export default changeStarted