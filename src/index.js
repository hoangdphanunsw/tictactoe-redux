import React from 'react';
import { render } from 'react-dom';
import './index.css';
import App from './components/App';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension';

// const store = createStore(rootReducer)
const store = createStore(rootReducer, composeWithDevTools())
// store.subscribe(() => console.log(store.getState()))


render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)
