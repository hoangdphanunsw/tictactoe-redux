// root reducer file
import { ADD_COUNTER, SUB_COUNTER } from '../constants/action_types'

const add = (state = 0, action) => {
    switch (action.type) {
        case ADD_COUNTER:
            return state+1

        case SUB_COUNTER:
            return state-1

        default:
            return state
    }
}

export default add