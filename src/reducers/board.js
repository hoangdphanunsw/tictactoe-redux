// root reducer file
import { PLACE_MOVE, RESTART_GAME } from '../constants/action_types'

const board = (state = new Array(9).fill(0), action) => {
    switch (action.type) {
        case PLACE_MOVE:
            if (state[action.index] == 0) {
                let newArray = Array(9)
                newArray = state
                newArray[action.index] = action.player
                return newArray
            } else {
                return state
            }
        case RESTART_GAME:
            return Array(9).fill(0)

        default:
            return state
    }
}

export default board