import React from 'react';
import '../App.css';
import Add from '../containers/Add'
// import Board from '../components/Board'
import Board from '../containers/BoardTiles'

const App = () => {
  return (
    <div className="App" style={{height: "100vh", display: "flex", alignItems: "center", justifyContent: "center", flex: 1}}>
      <Board />
    </div>
  );
}

export default App;
