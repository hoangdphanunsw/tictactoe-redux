import React from 'react'
import { button } from 'react'
import { connect } from 'react-redux'
import { add } from '../actions/index'

function Add({dispatch}) {

    return (
        <div>
            <button
                onClick={() => {dispatch(add())}}>
                Click to increase
            </button>
        </div>
    )
}

export default connect()(Add)